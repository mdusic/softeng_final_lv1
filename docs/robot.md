# Podešavanje Robot Framework-a
Robot Framework omogućava lagano testiranje i ponovnu upotrebu već napisanih datoteka
za testiranje funkcionalnosti ili strukture projekta. U sljedećih nekoliko odlomaka navedeno je 
kako se instalira Robot Frameowrk, kako se pišu testovi, kako se izvršavaju i koja je struktura.

Detaljnije upute mogu se pronaći na sljedećem linku: `http://robotframework.org/`


## Instalacija

Instalacija Robot Framework-a je moguća na sljedeći način
```
pip install robotframework
```

Dodavanje `Selenium2Library` biblioteke otvara mogućnosti testiranja grafičkog sučelja (primjerice 
pokretanje `web-browser`-a sa točno definiranim url-om.)

```
pip install --upgrade robotframework-selenium2library
```

Nadalje, potrebno je skinuti i `webdriver` za Firefox bez kojega neće biti moguće testirati grafičko sučelje

Na idućem linku je moguće skinuti `geckodriver` te ga je potrebno raspakirati u direktorij `Python` instalacije.

 `https://github.com/mozilla/geckodriver/releases`

### Primjer test suite-a
U `Settings` sekciji moguće je navesti datoteke odnosno module čije funkcije želimo koristiti.

Primjerice u test case-u `index.html should exist` korištena je funkcija `Get Asset Full Path` iz modula
`utils.py` (Robot Framework funkciju "pretvara" u `Keyword` po određenim pravilima). Ta funkcija dohvaća putanju do podatka kojeg navedemo kao argument, zatim spremamo
putanju do tog podatka u varijablu `${asset_path}` , te sa `File Should Exist` naredbom (koja dolazi u paketu s Robot Framework-om) 
kojoj kao argument predajemo putanju do podatka  provjeravamo postoji li doista taj podatak.

Ukoliko ne postoji, rezultat testa će biti `Fail` te je moguće u `reports` direktoriju vidjeti
razlog zašto test nije prošao.  
```
*** Settings ***
Library           OperatingSystem
Library           String
Library           Process
Library           ../../simplefss/utils.py

*** Test Cases ***
index.html should exist
    ${asset_path}=    Get Asset Full Path    www/index.html
    File Should Exist    ${asset_path}
    Log    ${asset_path}    warn
```

### Dodavanje testova unutar test suite-a
Dodavanje testova unutar pojedinog test suite-a se sastoji od modificiranja postojećeg test suite-a
na način da se u sekciju `Test Cases` doda naziv test case-a, a zatim njegova logika (potrebno je modificirati
i `Settings` sekciju ukoliko želimo koristiti custom `keywords`)

Dodavanje testova unutar postojećeg test suite-a je vidljivo na sljedećem primjeru.

```
*** Settings ***
Library           OperatingSystem
Library           String
Library           Process
Library           ../../simplefss/utils.py

*** Test Cases ***
index.html should exist
    ${asset_path}=    Get Asset Full Path    www/index.html
    File Should Exist    ${asset_path}
    Log    ${asset_path}    warn

index.html should not be empty
    ${asset_path}=    Get Asset Full Path    www/index.html
    File Should Exist    ${asset_path}
    Log    ${asset_path}    warn
    File Should Not Be Empty    ${asset_path}
```
Bitno je primijetiti da su razmaci vrlo bitni te će Robot javiti grešku ukoliko je pogreška u sintaksi.

Idući primjer pokazuje kako koristiti biblioteku za testiranje grafičkog sučelja (primjerice sadrži
li stranica određeni tekst ili link). 

`Za uspjeh dolje navedenog testa potrebno je da je server simpleFSS pokrenut`

```
*** Settings ***
Test Setup        Open test browser
Test Teardown     Close test browser
Library           OperatingSystem
Library           String
Library           Process
Library           Selenium2Library

*** Test Cases ***
index.html should contain text
    Open Browser    localhost:8080    firefox
    Maximize Browser Window
    Page Should Contain    simpleFSS
    [Teardown]    Close Browser

```


### Dodavanje test suite-ova
Dodavanje test suite-ova je omogućeno na način da se unutar direktorija `tests/ROBOT` 
doda file `<naziv_test_suite-a>` s prethodno opisanim načinom pisanja testova.

### Pokretanje testova

Za pokretanje testova pomoću Robot Framework-a koristi se sljedeća naredba 
```
robot -T -d reports putanja_projekta/tests/ROBOT/<određeni_test_suite>
```
Gdje je `putanja_projekta` putanja do kloniranog projekta, a `određeni_test_suite` naziv test suite-a kojeg 
želimo pokrenuti. 

Zastavica `-d reports` označava direktorij u kojem će se kreirati rezultati testova.

Moguće je pokrenuti sve test suite-ove na sljedeći način

```
robot -T -d reports <putanja_do_ROBOT_direktorija>
```