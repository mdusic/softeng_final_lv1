import pytest
import inspect, os, sys
from simplefss import utils
from simplefss import settings

def test_xSuke1():
	arg = "simplefss .".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_xSuke2():
	arg = "simplefss . --port".split(' ')
	assert utils.parse_args(arg) == (8080, False)

def test_xSuke3():
	arg = "simplefss . --port 1234".split(' ')
	assert utils.parse_args(arg) == (1234, False)

def test_xSuke4():
	arg = "simplefss . --readonly".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_xSuke5():
	arg = "simplefss . --readonly --port".split(' ')
	assert utils.parse_args(arg) == (8080, True)

def test_xSuke6():
	arg = "simplefss . --readonly --port 1".split(' ')
	assert utils.parse_args(arg) == (1, True)