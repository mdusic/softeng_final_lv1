import pytest
import datetime
import os
from simplefss.case_handlers import *

def get_asset_path( asset):
    tests_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(tests_path, "test_assets", asset)
    return file_path

def test_file_exists_true():
    file_path = get_asset_path("file.html")
    handler = CaseFileExistsHandler()
    assert handler.test(file_path) == True

def test_file_exists_false():
    file_path = get_asset_path("nepostoji.html")
    handler = CaseFileExistsHandler()
    assert handler.test(file_path) == False

def test_file_exists_run_true():
    file_path = get_asset_path("file.html")
    handler = CaseFileExistsHandler()
    assert "This file exists" in handler.run(file_path)

def test_file_exists_run_false():
    file_path = get_asset_path("nepostoji.html")
    handler = CaseFileExistsHandler()
    with pytest.raises(CaseError) as e:
        handler.run(file_path)
    assert e.value.error_code == 500


def test_file_not_exists_true():
    file_path = get_asset_path("file_1.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(file_path) == True


def test_file__not_exists_false():
    file_path = get_asset_path("file.html")
    handler = CaseFileNotExistsHandler()
    assert handler.test(file_path) == False

def test_file_not_exists_run_true():
    """ FileNotExistsHandler returns true for file that does not exist """
    file_path = get_asset_path("nepostojeci.html")
    handler = CaseFileNotExistsHandler()
    with pytest.raises(CaseError) as e:
        handler.run(file_path)
    assert e.value.error_code == 404

def test_file_not_exists_run_false():
    """ FileNotExistsHandler is not supposed to run if file exists. If file
    exists FileExistsHandler is supposed to fire. If this handler does fire
    than it's an internal server error"""
    file_path = get_asset_path("file.html")
    handler = CaseFileNotExistsHandler()
    with pytest.raises(CaseError) as e:
        handler.run(file_path)
    assert e.value.error_code == 500

def test_logger_create_error_if_not_exists():
    logger = ErrorLogger()
    logger.log('putanja', 'poruka', 123)
    assert os.path.isfile('./errors.log') == True
    with open('./errors.log') as f:
        data = f.readlines()
    zadnjaLinija = data[-1]
    assert zadnjaLinija.find('putanja') > -1
    assert zadnjaLinija.find('poruka') > -1
    assert zadnjaLinija.find('123') > -1
    datum = datetime.datetime.now()
    datumSlozeni = datum.strftime("%Y-%m-%d %H:%M")
    assert zadnjaLinija.find(datumSlozeni) > -1