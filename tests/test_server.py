import pytest
import requests
import os
from simplefss import server

def get_asset_path( asset):
    tests_path = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(tests_path, "test_assets", asset)
    return file_path

def test_post_request():
    "Request to url returns 200"
    url = 'http://127.0.0.1:8080/'
    files = {'file': open(get_asset_path("file.html"),'rb')}
    resp = requests.post(url, files=files)
    assert resp.status_code == 200

def test_access_log():
    """Request to url writes to access log"""
    url = 'http://127.0.0.1:8080/'
    files = {'file': open(get_asset_path("file.html"), 'rb')}
    resp = requests.post(url, files=files)
    with open('access.log', 'r') as f:
        with open('test.txt') as myfile:
            lastLine = (list(myfile)[-1])
    """check if log line exists"""
    assert lastLine != ""



